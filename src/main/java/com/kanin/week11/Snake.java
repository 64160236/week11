package com.kanin.week11;

public class Snake extends Animal implements Crawlabe{
    public Snake(String name) {
        super(name, 0);
        
    }

    @Override
    public void crawl() {
        System.out.println(this.toString() + " crawl");
        
    }

    @Override
    public void eat() {
        System.out.println(this.toString() + " eat");
        
    }

    @Override
    public void walk() {
        System.out.println(this.toString() + " can't walk");
        
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + " sleep");
        
    }
    @Override
    public String toString() {
        
        return "Snake(" +this.getName() + ")";
    }

}
