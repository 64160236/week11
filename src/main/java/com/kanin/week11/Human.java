package com.kanin.week11;

public class Human extends Animal implements Runable,Swimable,Crawlabe{
    public Human(String name) {
        super(name, 2);
        
    }

    @Override
    public void crawl() {
        System.out.println(this.toString() + " crawl");
        
    }

    @Override
    public void swim() {
        System.out.println(this.toString() + " swim");
        
    }

    @Override
    public void run() {
        System.out.println(this.toString() + " run");
        
    }

    @Override
    public void eat() {
        System.out.println(this.toString() + " eat");
        
    }

    @Override
    public void walk() {
        System.out.println(this.toString() + " walk");
        
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + " sleep");
        
    }
    @Override
    public String toString() {
       
        return "Human(" +this.getName() + ")";
    }
}
